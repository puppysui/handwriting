/**
 * 快速排序
 */
function quickSort(arr) {
    const len = arr.length;
    if (len < 2) {
        return arr
    }
    const target = arr[0]
    const left = []
    const right = []
    for (let i = 1; i < len; i++) {
        if (arr[i] < target) {
            left.push(arr[i])
        } else {
            right.push(arr[i])
        }
    }
    return quickSort(left).concat([target], quickSort(right))
}

console.log(quickSort([3, 5, 6, 2, 1]))


