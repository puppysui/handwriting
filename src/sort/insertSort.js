/**
 * 插入排序
 */
function insertionSort(arr) {
    const len = arr.length;
    let preIndex, current;
    for (let i = 1; i < len; i++) {
        preIndex = i - 1;
        current = arr[i];
        while (preIndex >= 0 && arr[preIndex] > current) {
            arr[preIndex + 1] = arr[preIndex];
            preIndex--; // 从后往前插入
        }
        arr[preIndex + 1] = current;
    }
    return arr;
}

let arr = [1, 4, 2, 3, 6]
console.log(insertSort(arr))