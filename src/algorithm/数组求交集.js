// 多个数组
const intersection = function (...args) {
    if (args.length === 0) {
        return []
    }
    if (args.length === 1) {
        return args[0]
    }
    return [...new Set(args.reduce((result, arg) => {
        return result.filter(item => arg.includes(item))
    }))]
};

// 两个数组
const intersection2 = function (nums1, nums2) {
    return [...new Set(nums1.filter((item) => nums2.includes(item)))]
};