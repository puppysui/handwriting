/**
 * 借用构造函数继承
 * 优点：1.避免了引用类型的属性被所有实例共享
 *      2.可以在创建子类实例时，向父类传参数
 * 缺点：1.每次创建子类实例都要调用父类方法
 *      2.只能继承父类的属性和方法，不能继承原型属性和方法
 */
function Parent(name) {
    this.name = name;
    this.baby = [1, 2, 3];
}

function Child(name) {
    Parent.call(this, name); // 核心代码
}

let child = new Child('dog');
child.baby.push(4);
console.log(child.name); // dog
console.log(child.baby); // [1, 2, 3, 4]

let child2 = new Child('cat')
console.log(child2.name); // cat
console.log(child2.baby); // [1, 2, 3]