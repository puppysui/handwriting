/**
 * 原型式继承
 * ES5中的 Object.create()
 * 缺点：1.和原型链继承一样，引用类型的属性被所有实例共享，数据被篡改
 *      2.创建子类实例时，不能向父类传参 
 */
function createObj(o) {
    function F() { }
    F.prototype = o
    return new F()
}

let person = {
    name: 'dog',
    baby: [1, 2, 3]
}

let p1 = createObj(person);
let p2 = createObj(person);

console.log(p1.baby); // [1, 2, 3, 4]
p1.baby.push(4);
console.log(p2.baby); // [1, 2, 3, 4]