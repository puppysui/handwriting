/**
 * 组合继承（原型链继承 + 借用构造函数继承）
 * 优点：1.解决引用类型属性被其他实例共享的问题
 *      2.子类可以向父类传参
 *      3.可以复用原型上的方法
 * 缺点：1.需要执行两次父类构造函数
 *      第一次是 Child.prototype = new Parent()
 *      第二次是 Parent.call(this, name)
 */
function Parent(name) {
    this.name = name;
    this.baby = [1, 2, 3];
}

Parent.prototype.sayName = function () {
    console.log(this.name);
}

function Child(name, age) {
    Parent.call(this, name); // 第二次调用Parent()
    this.age = age;
}

Child.prototype = new Parent(); // 第一次调用Parent()
Child.prototype.constructor = Child;
Child.prototype.sayAge = function () {
    console.log(this.age);
}

const c1 = new Child('dog', 18);
c1.baby.push(4);
console.log(c1.baby); // [1, 2, 3, 4]
c1.sayName(); // dog
c1.sayAge(); // 18

const c2 = new Child('cat', 20);
console.log(c2.baby); // [1, 2, 3]
c2.sayName(); // cat
c2.sayAge(); // 20