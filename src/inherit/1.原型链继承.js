/**
 * 原型链继承
 * 缺点：1.引用类型的属性被所有实例共享，数据被篡改
 *      2.创建子类实例时，不能向父类传参    
 */
function Parent() {
    this.age = 30;
    this.baby = [1, 2, 3];
}

Parent.prototype.getAge = function () {
    return this.age;
}

function Child() {
    this.name = 'dog';
}

Child.prototype = new Parent(); // 核心代码

Child.prototype.getName = function () {
    return this.name;
}

const child = new Child();
child.baby.push(4);
console.log(child.name); // dog
console.log(child.baby); // [1, 2, 3, 4]

const child2 = new Child();
console.log(child2.baby); // [1, 2, 3, 4]