/**
 * 寄生式继承
 * 缺点：1.和原型链继承一样，引用类型的属性被所有实例共享，数据被篡改
 *      2.创建子类实例时，不能向父类传参 
 */
 function createObj(o) {
    const clone = Object.create(o);
    clone.sayHi = function() {}
    return clone;
}

const person = {
    name: 'dog',
    baby: [1, 2, 3]
}

const p = createObj(person);
p.sayHi();