function MyPromise(executor) {
    this.status = 'pending'; // 初始状态
    this.success = undefined
    this.error = undefined
    let that = this;

    that.onfulfilledCallback = [];
    that.onrejectedCallback = [];

    function resolve(val) {
        if (that.status === 'pending') {
            that.status = 'fulfilled'
            that.success = val
            that.onfulfilledCallback && that.onfulfilledCallback(that.success)
        }
    }

    function reject(val) {
        if (that.status === 'pending') {
            that.status = 'rejected'
            that.error = val
            that.onrejectedCallback && that.onrejectedCallback(that.error)
        }
    }

    executor(resolve, reject)

}

MyPromise.prototype.then = function (handleFulfilled, handleRejected) {
    let that = this;
    if (this.status === 'fulfilled') {
        handleFulfilled(this.success) // 成功的值
    }
    if (this.status === 'rejected') {
        handleRejected(this.error) // 失败的值
    }
    if (this.status === 'pending') {
        that.onfulfilledCallback = handleFulfilled // 将处理函数存下来
        that.onrejectedCallback = handleRejected // 将处理函数存下来
    }
}
MyPromise.prototype.catch = function () { }
MyPromise.prototype.all = function () { }
MyPromise.prototype.race = function () { }

let mypromise = new MyPromise((resolve, reject) => {
    setTimeout(() => {
        resolve(1)
    }, 2000)
})
mypromise.then(res => {
    console.log(res)
}, err => {
    console.log(err);
})