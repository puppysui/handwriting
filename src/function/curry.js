/**
 * curry
 * 实现思路：
 * 1. 如果传入的 args 长度与原函数的 func.lenth (参数个数) 相同或者更长，直接调用即可
 * 2. 如果参数数量比原函数少参数少，则返回另一个包装器，重新应用 curried，把之前的参数和新的参数一起传入（保存在词法环境中）
 * 3. 最终递归到参数数量和原函数参数一样的情况，返回原函数调用 curried（1, 2, 3）
 */

function curry(func) {
    return function curried(...args) {
        // 传入的参数 和 原函数的参数个数 进行比较
        if (args.length >= func.length) {
            return func.apply(this, args)
        } else {
            return function(...args2) { // 偏函数
                return curried.apply(this, args.concat(args2))
            }
        }
    }
}

function sum(a, b, c) {
    return a + b + c
}

let curriedSum = curry(sum)

console.log(curriedSum(1, 2, 3));
console.log(curriedSum(1)(2, 3));
console.log(curriedSum(1)(2)(3));
