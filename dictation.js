/**
 * 我的默写
 */

function debounce(func, wait = 50) {
    let timer;
    return function (...args) {
        if (timer) clearTimeout(timer)
        timer = setTimeout(() => {
            func.apply(this, args)
        }, wait)
    }
}

function throttle(func, wait = 50) {
    let last = 0
    return function (...args) {
        let now = +new Date()
        if (now - last > wait) {
            func.apply(this, args)
            last = now
        }
    }
}

function mynew(func, ...args) {
    let instance = Object.create(func.prototype);
    let result = func.apply(instance, args)
    return typeof result === 'object' ? result : instance;
}

function myinstanceof(left, right) {
    let proto = Object.getPrototypeOf(left)
    while (true) {
        if (proto === null) return false
        if (proto === right.prototype) return true;
        proto = Object.getPrototypeOf(proto)
    }
}

Function.prototype.myapply = (content = window, ...args) => {
    content.fn = this;
    let result = content.fn(args)
    delete content.fn
    return result
}

Function.prototype.mycall = (content = window, ...args) => {
    content.fn = this;
    let result = content.fn(...args)
    delete content.fn
    return result
}

Function.prototype.mybind = (content = window, ...args) => {
    return (...newArgs) => this.apply(content, [...args, ...newArgs])
}

function create(proto) {
    function Fn() { }
    Fn.prototype = proto;
    return new Fn();
}

// extend
function Parent() { }

function Child() {
    Parent.apply(this)
}

Child.prototype = Object.create(Parent.prototype)
Child.prototype.constructor = Child

Promise.prototype.all = function (promises) {
    return new Promise((resolve, reject) => {
        let result = []
        let index = 0
        let len = promises.length
        if (len === 0) {
            resolve(result);
            return;
        }
        for (let i = 0; i < len; i++) {
            // promise[i]可能不是一个promise
            Promise.resolve(promises[i]).then(data => {
                result[i] = data;
                index++;
                if (index === len) resolve(result)
            }).catch(err => {
                reject(err)
            })
        }
    })
}

Promise.prototype.finally = function (callback) {
    this.then(value => {
        return Promise.resolve(callback()).then(() => {
            return value;
        })
    }, error => {
        return Promise.resolve(callback()).then(() => {
            throw error
        })
    })
}

Promise.prototype.race = function (promises) {
    return new Promise((resolve, reject) => {
        let len = promises.length
        if (len === 0) return;
        for (let i = 0; i < len; i++) {
            Promise.resolve(promises[i]).then(data => {
                resolve(data)
                return;
            }).catch(err => {
                reject(err)
                return;
            })

        }
    })
}

class MyPromise {
    constructor(fn) {

        this.state = 'pending';
        this.value = ''

        this.resolveCallbacks = []
        this.rejectCallbacks = []

        fn(this.resolve.bind(this), this.reject.bind(this))
    }

    resolve(value) {
        if (this.state === 'pending') {
            this.state = 'resolved'
            this.value = value
            this.resolveCallbacks.map(cb => cb(value))
        }
    }

    reject(value) {
        if (this.state === 'pending') {
            this.state = 'rejected'
            this.value = value
            this.rejectCallbacks.map(cb => cb(value))
        }
    }

    then(onResolved, onRejected) {
        if (this.state === 'pending') {
            this.resolveCallbacks.push(onResolved)
            this.rejectCallbacks.push(onRejected)
        }
        if (this.state === 'resolved') onResolved(this.value)
        if (this.state === 'rejected') onRejected(this.value)
    }
}

function curry(func) {
    return function curried(...args) {
        // 传入的参数 和 原函数的参数个数 进行比较
        if (args.length >= func.length) {
            return func.apply(this, args)
        } else {
            return function (...args2) { // 偏函数
                return curried.apply(this, args.concat(args2))
            }
        }
    }
}




















